import React, {Fragment} from "react";
import {useSelector} from "react-redux";

const ResultsComponent = ()=> {
   const {form, results} = useSelector((state:IRoot)=>state);
    function refineResults() {
        return results.raw.filter(item => item.quote.toLowerCase().indexOf(form.keyPhrase.toLowerCase()) > -1)
    }

    if(!results.notTouched){
        const {character, keyPhrase} = form;
        const refined = refineResults();
        if (keyPhrase.length > 0) {
            return refined.length > 0 ? <Fragment>
                <ul>
                    {
                        refined.map((item, index) => {
                            return <li key={index}>{item.quote}</li>
                        })
                    }
                </ul>
            </Fragment> : <p>{`Sorry! "${character}" has no quotes about "${keyPhrase}".`}</p>
        }
        return <ul>
            {
                results.raw.map((item, index) => {
                    return <li key={index}>{item.quote}</li>
                })
            }
        </ul>
    }
    return <Fragment />
};



export default ResultsComponent;
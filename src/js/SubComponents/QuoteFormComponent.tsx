import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {submitFormAction} from "../Store/actions/submitFormAction";
import {updateInputAction} from "../Store/actions/updateInputAction";
import {updateCharacterAction} from "../Store/actions/updateCharacterAction";
import {showErrorAction} from "../Store/actions/showErrorAction";
import {characters} from "../utils/constants";

const QuoteFormComponent = (props: any) => {
    const {form, results} = useSelector((state: IRoot) => state);
    const dispatch = useDispatch();

    function submitForm(payload: string) {
        dispatch(submitFormAction(payload))
    }

    function showError() {
        dispatch(showErrorAction())
    }

    function updateInput(payload: string) {
        dispatch(updateInputAction(payload));
    }

    function updateCharacter(payload: string) {
        dispatch(updateCharacterAction(payload));
    }


    function searchQuote(e: any) {
        e.preventDefault();
        const {character} = form;
        const {notTouched} = results;
        if (notTouched) {
            character.length > 0 ? submitForm(character.toLowerCase().replace(/\s/g, '-')) : showError()
        }

    }

    function saveInput(e: any) {
        updateInput(e.target.value)
    }

    function selectCharacter(e: any) {
        updateCharacter(e.target.value)

    }

    return (
        <form method="post" onSubmit={searchQuote}>
            <fieldset>
                <input onChange={selectCharacter}
                       list="characters"
                       type="text"
                       className={form.noCharacterSelected ? 'err' : ''}
                       placeholder="Type a character name ... "/>
                <datalist id="characters">
                    {
                        characters.map((item, index) => {
                            return <option key={index}>{item}</option>
                        })
                    }
                </datalist>
            </fieldset>
            <fieldset>
                <input
                    type="text"
                    placeholder="Look for quote ..."
                    onInput={saveInput}
                />
                <input type="submit" value="go"/>
            </fieldset>
        </form>
    )
};


export default QuoteFormComponent;
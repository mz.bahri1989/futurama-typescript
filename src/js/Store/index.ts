import {applyMiddleware, createStore} from "redux";
import createSagaMiddleware from 'redux-saga';
import rootReducer from "./Reducers";
import rootSaga from "./Saga";

const sagaMiddleware = createSagaMiddleware();

// Develop Mode
/*const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(sagaMiddleware)));

//Production Mode*/
const store = createStore(rootReducer,applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);
export default store;
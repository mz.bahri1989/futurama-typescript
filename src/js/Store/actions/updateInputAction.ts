import {_UPDATE_SEARCH_FIELD} from "../types";

export function updateInputAction(payload = ''){
    return {
        type: _UPDATE_SEARCH_FIELD,
        payload
    }
}
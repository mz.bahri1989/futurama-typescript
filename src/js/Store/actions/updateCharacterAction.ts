import {_UPDATE_CHARACTER_FIELD} from "../types";

export function updateCharacterAction(payload = ''){
    return {
        type: _UPDATE_CHARACTER_FIELD,
        payload
    }
}
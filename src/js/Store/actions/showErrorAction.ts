import {_CHARACTER_ERR} from "../types";

export function showErrorAction(payload=''){
    return{
        type:_CHARACTER_ERR,
        payload
    }
}
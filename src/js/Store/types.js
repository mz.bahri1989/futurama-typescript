export const _UPDATE_SEARCH_FIELD = 'update-search-field';
export const _GET_DATA = "get-data-from-api";
export const _DATA_RECEIVED = 'data-received';
export const _UPDATE_CHARACTER_FIELD = 'update-character-field';

export const _CHARACTER_ERR = 'select-character';


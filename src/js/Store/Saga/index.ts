import { all } from 'redux-saga/effects';
import {getDataWatcher} from "./getDataSaga";


export default function* rootSaga() {
    yield all([
        getDataWatcher(),
    ])
}
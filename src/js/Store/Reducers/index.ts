import {combineReducers} from "redux";
import {ResultsReducer} from "./ResultsReducer";
import {FormReducer} from "./FormReducer";

const rootReducer = combineReducers({
    results: ResultsReducer,
    form: FormReducer
});
export default rootReducer
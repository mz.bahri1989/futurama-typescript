import React from "react";
import ResultsComponent from "../SubComponents/ResultsComponent";
import {useSelector} from "react-redux";
import QuoteFormComponent from "../SubComponents/QuoteFormComponent";

import "../../css/index.scss";

const HomeComponent = ()=> {
const {raw} = useSelector((state: IRoot) => state.results);
    return (
        <main>
            <h1>FUTURAMA</h1>
            <p>Select Your Favorite Character to Read Their Quotes</p>
            <QuoteFormComponent />
            {raw.length > 0 ? <ResultsComponent/>:''}
        </main>
    )
};

export default HomeComponent;
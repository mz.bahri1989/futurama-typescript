interface Action {
    type: string;
    payload:any;
}
interface IResults{
    raw:Array<any>;
    notTouched: boolean;
}
interface IForm{
    keyPhrase: string;
    character: string;
    noCharacterSelected: boolean
}

interface IRoot {
    results: IResults;
    form: IForm;
}
interface IHomeProps {
    results:IResults
}
interface IResultsProps{
    results: IResults;
    form: IForm;
}
interface IQuotesProps {
    results: IResults;
    form: IForm;
    submitFormAction: (payload: string)=>Action;
    showErrorAction: ()=>Action;
    updateInputAction:(payload: string)=>Action
    updateCharacterAction:(payload: string)=>Action
}

interface IRequest {
    method:string;
    url:string;
    data?:any;
    headers?:{
        [key:string]:string;
    }
}

